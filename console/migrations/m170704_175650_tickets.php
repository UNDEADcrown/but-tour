<?php

use yii\db\Migration;

class m170704_175650_tickets extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tickets}}', [
            'id' => $this->primaryKey(),
            'festival_id' => $this->integer(),
            'type' => $this->string()->notNull(),
            'description' => $this->string(),
            'price' => $this->string(),
            'status' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170704_175650_tickets cannot be reverted.\n";

        return false;
    }

}
