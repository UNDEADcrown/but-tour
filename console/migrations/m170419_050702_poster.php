<?php

use yii\db\Migration;

class m170419_050702_poster extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%poster}}', [
            'id' => $this->primaryKey(),
            'festivalId' => $this->integer()->notNull(),
            'date' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'location_from' => $this->string()->notNull(),
            'location_to' => $this->string()->notNull(),
            'area' => $this->string()->notNull(),
            'avatar' => $this->string(),
            'form_fields' => $this->string(500)->notNull(),
            'form_required_fields' => $this->string(500)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%poster}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
