<?php

use yii\db\Migration;

class m180305_182223_posterAddUserId extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn(poster, "curators", "varchar(255)");
    }

    public function down()
    {
        echo "m180305_182223_posterAddUserId cannot be reverted.\n";
        $this->dropColumn(poster, "curators");
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180305_182223_posterAddUserId cannot be reverted.\n";

        return false;
    }
    */
}
