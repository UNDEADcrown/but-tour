<?php

use yii\db\Migration;

class m170517_044605_requests extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%requests}}', [
            'id' => $this->primaryKey(),
            'author' => $this->string()->notNull(),
            'festival_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'surname' => $this->string()->notNull(),
            'patronymic' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'phone_number' => $this->string(),
            'social_links' => $this->string(),
            'document_type' => $this->string(),
            'document_number' => $this->string(),
            'date_of_issue' => $this->string(),
            'who_gave' => $this->string(),
            'validity' => $this->string(),
            'status' => $this->string(),
            'package_type' => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%requests}}');
    }
}
