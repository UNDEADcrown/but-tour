<?php

use yii\db\Migration;

class m180227_181649_requestsUpdate extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn(requests, "ticket_id", "integer");
    }

    public function down()
    {
        echo "m180227_181649_requestsUpdate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180227_181649_requestsUpdate cannot be reverted.\n";

        return false;
    }
    */
}
