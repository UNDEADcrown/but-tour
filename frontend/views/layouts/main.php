<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use common\models\LoginForm;
use common\models\User;

AppAsset::register($this);

$loginModel = new LoginForm();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Разработка',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Фестивали', 'url' => ['/site/festivals']],
        ['label' => 'Афиша', 'url' => ['/site/posters']],
    ];
    if(User::isAdmin()){
        $menuItems[] = ['label' => 'Админка', 'items' => [
                ['label' => 'Фестивали', 'url' => '/admin/festivals'],
                ['label' => 'Афиша', 'url' => '/admin/poster'],
                ['label' => 'Заявки', 'url' => '/admin/requests'],
                ['label' => 'Билеты', 'url' => '/admin/tickets'],
                ['label' => 'Пользователи', 'url' => '/admin/user']
        ]];
    }
    if(User::isModerator()){
        $menuItems[] = ['label' => 'Админка', 'items' => [
            ['label' => 'Заявки', 'url' => '/admin/requests']
        ]];
    }
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
        $menuItems[] = '<li><a data-toggle="modal" data-target="#auth-modal" id="auth-modal-link">Войти</a></li>';
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                '(' . Yii::$app->user->identity->username . ') Выйти',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Разработка <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php require_once Yii::getAlias("@app") . '/views/helpers/authModal.php'; ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
