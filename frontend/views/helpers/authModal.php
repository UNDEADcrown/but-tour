<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\LoginForm;;

$loginModel = new LoginForm();
?>
<div class="modal fade" id="auth-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>Через социальные сети:</h4>
                <?= \nodge\eauth\Widget::widget(array('action' => 'site/login')); ?>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(['action' =>['site/login'], 'id' => 'login-form']); ?>

                <?= $form->field($loginModel, 'username')->textInput()->label('Email') ?>

                <?= $form->field($loginModel, 'password')->passwordInput()->label('Пароль') ?>

                <?= $form->field($loginModel, 'rememberMe')->checkbox()->label('Запомнить меня') ?>

                <div style="color:#999;margin:1em 0">
                    Вы можете <?= Html::a('восстановить пароль', ['site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>