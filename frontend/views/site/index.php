<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>

    <div class="wrapper-index-page">
    <div class="row">
        <div class="col-sm-7 slider-news">


            <div class="index-slider owl-carousel">
                <div> <?= Html::img('@web/images/our-space.jpg');?> </div>
                <div> <?= Html::img('@web/images/rock-za-bobrov.jpg');?> </div>
            </div>


        </div>
        <div class="col-sm-5 box-news">
            <div class="title-box-news">
                <span>новости</span>
            </div>
            <div class="mini-news">
                <div class="row">
                    <div class="col-sm-4 img-box-news">
                        <?= Html::img('@web/images/uploads/festive-news.png');?>
                    </div>
                    <div class="hover-shadow">
                    </div>
                    <div class="col-sm-8 text-box-news">
                        <span>
                            Юбилейная Пиратская Станция «History»
                            пройдет 21 октября в Москве. Билеты в продаже!
                        </span>
                        <br>
                        <span>2 мая</span>
                    </div>
                </div>
            </div>
            <div class="mini-news">
                <div class="row">
                    <div class="col-sm-4 img-box-news">
                        <?= Html::img('@web/images/uploads/festive-news.png');?>
                    </div>
                    <div class="hover-shadow">
                    </div>
                    <div class="col-sm-8 text-box-news">
                        <span>
                            Юбилейная Пиратская Станция «History»
                            пройдет 21 октября в Москве. Билеты в продаже!
                        </span>
                        <br>
                        <span>2 мая</span>
                    </div>
                </div>
            </div>
            <div class="mini-news">
                <div class="row">
                    <div class="col-sm-4 img-box-news">
                        <?= Html::img('@web/images/uploads/festive-news.png');?>
                    </div>
                    <div class="hover-shadow">
                    </div>
                    <div class="col-sm-8 text-box-news">
                        <span>
                            Юбилейная Пиратская Станция «History»
                            пройдет 21 октября в Москве. Билеты в продаже!
                        </span>
                        <br>
                        <span>2 мая</span>
                    </div>
                </div>
            </div>
            <div class="mini-news">
                <div class="row">
                    <div class="col-sm-4 img-box-news">
                        <?= Html::img('@web/images/uploads/festive-news.png');?>
                    </div>
                    <div class="hover-shadow">
                    </div>
                    <div class="col-sm-8 text-box-news">
                        <span>
                            Юбилейная Пиратская Станция «History»
                            пройдет 21 октября в Москве. Билеты в продаже!
                        </span>
                        <br>
                        <span>2 мая</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>