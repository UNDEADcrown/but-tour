<?php

/* @var $this yii\web\View */
/* @var $collection app\models\Festivals */

use yii\helpers\Html;
use common\models\User;

$this->title = 'Фестивали и Мероприятия';
?>
<div class="site-festivals">

    <div class="row">

        <?php foreach ($collection as $festival): ?>

            <div class="col-sm-12">
                <div class="panel panel-default festival">
                    <div class="panel-body row">
                        <div class="col-sm-4">
                            <?= Html::img('@web/uploads/festivals/'.$festival['logo']);?>
                        </div>
                        <div class="cols-sm-8 festival-right">
                            <h2 class="festival-name"><?php echo $festival['name']; ?></h2>
                            <div class="festival-description">
                                <?php echo $festival['short_description']; ?>
                            </div>
                            <a href="/festival?id=<?php echo $festival['id']; ?>">
                                <button type="button" class="btn btn-default festival-read-more">Читать далее</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>

    </div>
</div>
