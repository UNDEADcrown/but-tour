<?php

/* @var $this yii\web\View */
/* @var $poster app\models\Poster */

use yii\helpers\Html;
use common\models\User;
use yii\helpers\Url;
use kartik\markdown\Markdown;
use app\models\Tickets;

$this->title = 'Фестиваль ' . $poster->name;
?>
<div class="site-festival">

    <div class="poster-head" style="background-image: url('<?php echo Url::to($poster->getPosterBackground()); ?>')">
        <h1><?= $poster->name; ?></h1>
    </div>

    <div class="poster-information">
        <div class="pi-item">
            <div class="pi-title">Когда</div>
            <div class="pi-value">
                <span>
                    <?= str_replace(array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ),array( 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря' ), Yii::$app->formatter->asDate($poster->date, 'php:d F Y')) ; ?>
                </span>
            </div>
        </div>
        <div class="pi-item">
            <div class="pi-title">Место</div>
            <div class="pi-value"><span><?= $poster->location_to; ?></span></div>
        </div>
        <div class="pi-item">
            <div class="pi-title">Площадка</div>
            <div class="pi-value"><span><?= $poster->area; ?></span></div>
        </div>
        <div class="pi-item">
            <div class="pi-title">Заказ</div>
            <div class="pi-value">
                <span>
                    <input data-toggle="modal" data-target="#new-request-modal" class="request-modal-link btn btn-success" data-poster-id="<?= $poster->id; ?>" type="button" value="Оформить заявку">
                </span>
            </div>
        </div>
    </div>

    <div class="poster-description">
        <?= Markdown::convert($poster->description); ?>
    </div>

    <div class="modal request-modal fade" id="new-request-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>Оформление заявки: <?php echo $poster->name; ?></h4>
                </div>
                <div class="modal-body">
                    <?php $tickets = Tickets::getTicketsByFestivalId($poster->id); ?>
                    <?php if (count($tickets) > 0 ): ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Тип билета</th>
                                <th>Цена</th>
                                <th>Количество</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tickets as $ticket): ?>
                                <tr class="ticket-info" data-id="<?= $ticket['id']; ?>">
                                    <td class="ticket-type"><?= $ticket['type']; ?></td>
                                    <td class="ticket-price"><?= $ticket['price']; ?></td>
                                    <td>
                                        <div class="ticket-buttons-set">
                                            <input type="button" class="ticket-minus" value="-">
                                            <input type="number" min="0" max="10" class="ticket-count" value="0">
                                            <input type="button" class="ticket-plus" value="+">
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td class="report-tickets-count" colspan="3">
                                    Выбрано билетов: <span>0</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="report-tickets-price" colspan="3">
                                    На сумму: <span>0</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-info continue-request-button">Продолжить >>></button>
                        <?= Html::beginForm(['', 'id' => 'request-form'], 'post', ['enctype' => 'multipart/form-data']) ?>
                        <div class="ticket-request-forms">

                        </div>
                        <?= Html::endForm() ?>
                    <?php else: ?>
                        Для данного мероприятия отсутствуют билеты.
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</div>
