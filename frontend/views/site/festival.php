<?php

/* @var $this yii\web\View */
/* @var $festival app\models\Festivals */

use yii\helpers\Html;
use common\models\User;

$this->title = 'Фестиваль ' . $festival->name;
?>
<div class="site-festival">

    <h1><?= $festival->name; ?></h1>

    <div class="festival-description">
        <?= $festival->description; ?>
    </div>

</div>
