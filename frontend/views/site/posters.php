<?php

/* @var $this yii\web\View */
/* @var $collection app\models\Poster */

use yii\helpers\Html;
use app\models\Tickets;

$this->title = 'Афиша';
?>
<div class="site-posters">

    <div class="row">

        <?php foreach ($collection as $poster): ?>

            <div class="col-sm-12">
                <div class="panel panel-default poster">
                    <div class="panel-body row">
                        <div class="poster-date">
                            <?= str_replace(array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ),array( 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ), Yii::$app->formatter->asDate($poster["date"], 'php:F d')) ; ?>
                        </div>
                        <div class="poster-name">
                            <?= $poster["name"]; ?>
                            <span class="poster-include-cities">
                                <?= $poster['location_from']; ?>
                            </span>
                        </div>
                        <div class="poster-location">
                            <?= $poster["location_to"]; ?>
                            <span class="poster-area">
                                <?= $poster['area']; ?>
                            </span>
                        </div>
                        <div class="more-info">
                            <a href="/poster?id=<?= $poster['id']; ?>"><button class="btn btn-info">Подробнее</button></a>
                        </div>
                        <div class="get-request">
                            <a data-toggle="modal" data-target="#new-request-modal-<?php echo $poster['id']; ?>" class="request-modal-link" data-field="<?= $poster['form_fields']; ?>" data-poster-id="<?= $poster['id']; ?>"><button class="btn btn-success">Оформить</button></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal request-modal fade" id="new-request-modal-<?php echo $poster['id']; ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4>Оформление заявки: <?php echo $poster['name']; ?></h4>
                        </div>
                        <div class="modal-body">
                            <?php $tickets = Tickets::getTicketsByFestivalId($poster['id']); ?>
                            <?php if (count($tickets) > 0 ): ?>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Тип билета</th>
                                        <th>Цена</th>
                                        <th>Количество</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($tickets as $ticket): ?>
                                            <tr class="ticket-info" data-id="<?= $ticket['id']; ?>">
                                                <td class="ticket-type"><?= $ticket['type']; ?></td>
                                                <td class="ticket-price"><?= $ticket['price']; ?></td>
                                                <td>
                                                    <div class="ticket-buttons-set">
                                                        <input type="button" class="ticket-minus" value="-">
                                                        <input type="number" min="0" max="10" class="ticket-count" value="0">
                                                        <input type="button" class="ticket-plus" value="+">
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td class="report-tickets-count" colspan="3">
                                                Выбрано билетов: <span>0</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="report-tickets-price" colspan="3">
                                                На сумму: <span>0</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="button" class="btn btn-info continue-request-button">Продолжить >>></button>
                                <?= Html::beginForm(['', 'id' => 'request-form'], 'post', ['enctype' => 'multipart/form-data']) ?>
                                    <div class="ticket-request-forms">

                                    </div>
                                <?= Html::endForm() ?>
                            <?php else: ?>
                                Для данного мероприятия отсутствуют билеты.
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>

<div class="ticket-request-form-template">
    <div class="request-form">
        <span class="label label-info">Участник #<span>{{form_number}}</span></span>
        <!-- TODO: нужно сделать чтобы генерировалось с помощью js -->
        <select class="select-template" name="">
            <option value=""></option>
        </select>
        <input type="text" name="[form_{{form_number}}][email]" placeholder="Эл. Почта">
        <input type="text" name="[form_{{form_number}}][name]" placeholder="Имя">
        <input type="text" name="[form_{{form_number}}][surname]" placeholder="Фамилия">
        <input type="text" class="phone-input" name="[form_{{form_number}}][phone]" placeholder="Номер телефона">
    </div>
</div>