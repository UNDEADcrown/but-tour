<?php

namespace app\components;

use app\components\SiteHelper;

class FormsHelper
{
    public static function getCheckboxes($values, $label, $activeFields = null, $requiredFields = null){

        if(empty($activeFields)) {
            $activeFields = FormsHelper::getDefaultFields();
        } else {
            $activeFields = explode('|', $activeFields);
        }

        if(!empty($requiredFields)){
            $requiredFields = explode('|', $requiredFields);
        }

        echo  "<label class=\"control-label\">$label</label><br>";

        foreach (explode('|', $values) as $value){

            $isChecked = in_array($value, $activeFields) ? 'checked' : '';

            $isRequired = '';
            if(!empty($requiredFields) && in_array($value, $requiredFields)) {
                $isRequired = 'checked';
            }

            echo '<input type="checkbox" name="checkboxarray[' . $value . ']" ' . $isChecked . '>' . SiteHelper::getRequestFieldNameByCode($value) . '</input>&nbsp&nbsp;<input type="checkbox" name="checkboxarrarequired[' . $value . ']" ' . $isRequired . '><sup>*</sup></input><br>';

        }

    }

    public static function getDefaultFields(){
        return ['name', 'surname', 'email', 'phone_number'];
    }
}
