<?php

namespace app\components;

class SiteHelper
{
    public static function getRequestsFields(){

        return 'name|surname|patronymic|email|phone_number|social_links|document_type|document_number|date_of_issue|who_gave|validity';

    }

    public static function getRequestFieldNameByCode($code){

        switch($code){
            case "name": return "Имя"; break;
            case "surname": return "Фамилия"; break;
            case "patronymic": return "Отчество"; break;
            case "email": return "Email"; break;
            case "phone_number": return "Номер телефона"; break;
            case "social_links": return "Ссылки на соц. сети"; break;
            case "document_type": return "Тип документа"; break;
            case "document_number": return "Серия и номер документа"; break;
            case "date_of_issue": return "Дата выдачи"; break;
            case "who_gave": return "Кем выдан"; break;
            case "validity": return "Срок действия"; break;
        }

    }

    public static function getRequestsStatuses() {
        return ['Новая', 'В работе', 'Бронь', 'Оплачено', 'Отклонено'];
    }
}
