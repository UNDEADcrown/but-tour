<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/global.css',
        'css/owl.carousel.css',
        'css/owl.theme.default.css',
        'css/font-awesome.css',
    ];
    public $js = [
        'js/custom.js',
        'js/owl.carousel.min.js',
        'js/jquery.phonemask.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
