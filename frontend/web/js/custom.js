(function($){
    var initDealTab = function() {
        var properties = {
            openRequestModal: $(".get-request a"),
            fieldsToFilling: [],
            fieldsToFilter: $(".form-group"),
            posterId: $(".js-poster-id")
        };

        var methods = {
            run: function () {
                methods.startListeners();
            },
            startListeners: function () {
                properties.openRequestModal.on('click', methods.requestModalObserver);
            },
            requestModalObserver: function() {
                properties.fieldsToFilling = $(this).data('field').split('|');
                properties.fieldsToFilter.each(function(){
                    var field = $(this).find('input');
                    if(field.hasClass("jsObserver")) {
                        var fieldName = field.attr("id").substring(9);
                        if ($.inArray(fieldName, properties.fieldsToFilling) != -1) {
                            $(this).show();
                            field.val("");
                        } else {
                            $(this).hide();
                            field.val("null");
                        }
                    }
                });
                properties.posterId.val($(this).data('poster-id'));
            }
        };

        return methods;
    };

    initDealTab().run();

    $(document).ready(function(){
        $(".index-slider").owlCarousel({
            items: 1,
            loop: false,
            dots: true,
            margin: 10,
            autoplay: true
        });
    });

    var initTicketsPopup = function() {
        var properties = {
            currentModalWindow: '',
            openModalButton: $('.request-modal-link'),
            decreaseValueButton: $(".ticket-minus"),
            increaseValueButton: $(".ticket-plus"),
            countTicketsField: $(".ticket-count"),
            countTicketsFieldSelector: '.ticket-count',
            ticketsPriceSelector: '.ticket-price',
            ticketsTypeSelector: '.ticket-type',
            ticketRequestFormSelector: '.ticket-request-forms',
            totalTicketsCount: $(".report-tickets-count span"),
            totalTicketsPrice: $(".report-tickets-price span"),
            ticketInfoSelector: '.ticket-info',
            removeMemberSelector: '.remove-member',
            continueRequestButton: $('.continue-request-button'),
            totals: {
                ticketsCount: 0,
                totalPrice: 0,
                ticketsPerType: {
                    ticketIds: [],
                    ticketsCount: []
                }
            }
        };

        var methods = {
            run: function () {
                methods.startListeners();
            },
            startListeners: function () {
                properties.openModalButton.on('click', methods.setCurrentModal);
                properties.decreaseValueButton.on('click', methods.decreaseTicketsCount);
                properties.increaseValueButton.on('click', methods.increaseTicketsCount);
                properties.countTicketsField.on('change', methods.checkTicketCountValue);
                properties.continueRequestButton.on('click', methods.continueRequest);
            },
            setCurrentModal: function() {
                properties.currentModalWindow = $($(this).data('target'));
            },
            getTotals: function() {
                properties.totals.ticketsCount = properties.totals.totalPrice = 0;
                properties.totals.ticketsPerType.ticketIds = [];
                properties.totals.ticketsPerType.ticketsCount = [];
                properties.currentModalWindow.find(properties.ticketInfoSelector).each(function(){
                    var currentTicketsCount = parseInt($(this).find(properties.countTicketsFieldSelector).val());
                    properties.totals.ticketsCount += currentTicketsCount;
                    //TODO: тут нужно будет подставить регулярное выражение, вычисляющие другие валюты
                    properties.totals.totalPrice += parseInt($(this).find(properties.ticketsPriceSelector).text()) * currentTicketsCount ;
                    properties.totals.ticketsPerType.ticketsCount.push(currentTicketsCount);
                    properties.totals.ticketsPerType.ticketIds.push($(this).data('id'));
                });
            },
            decreaseTicketsCount: function() {
                var input = $(this).next();
                var currentValue = input.val();
                input.val(currentValue > 0 ? --currentValue : 0);
                methods.refreshTotals();
            },
            increaseTicketsCount: function() {
                var input = $(this).prev();
                var currentValue = input.val();
                input.val(currentValue < 10 ? ++currentValue : 10);
                methods.refreshTotals();
            },
            checkTicketCountValue: function() {
                var currentValue = $(this).val();
                if(currentValue > 10) {
                    $(this).val(10);
                } else if (currentValue < 0 || !$.isNumeric(currentValue)) {
                    $(this).val(0);
                }
                methods.refreshTotals();
            },
            refreshTotals: function() {
                methods.getTotals();
                properties.totalTicketsCount.html(properties.totals.ticketsCount);
                properties.totalTicketsPrice.html(properties.totals.totalPrice);
            },
            getTicketsData: function() {
                var tickets = [];
                properties.currentModalWindow.find(properties.ticketInfoSelector).each(function(){
                    var ticketId = $(this).data('id');
                    var ticketType = $(this).find(properties.ticketsTypeSelector).text() + ' (' + $(this).find(properties.ticketsPriceSelector).text() + ')';
                    tickets[ticketId] = ticketType;
                });
                return tickets;
            },
            continueRequest: function() {
                if (properties.totals.ticketsCount < 1) {
                    alert('Должен быть выбран хотябы 1 билет.');
                    return false;
                }

                var wrapper = properties.currentModalWindow.find(properties.ticketRequestFormSelector);
                var html = '';
                wrapper.html('');
                var i = 1;
                var ticketsData = methods.getTicketsData();
                for(var ticketTypes = 0; ticketTypes < properties.totals.ticketsPerType.ticketIds.length; ticketTypes++) {
                    for (var tickets = 1; tickets <= properties.totals.ticketsPerType.ticketsCount[ticketTypes]; tickets++) {
                        html = $('<div class="request_form_' + i + '"></div>');
                        html.addClass('request_form_' + i);
                        html.append('<span class="label label-info">Участник №'+i+'</span><i class="remove-member glyphicon glyphicon-remove"></i><br>');
                        html.append('<label>Тип билета:</label><br>');
                        html.append('<select class="form-control" name="form_' + i + '[ticket_type]"></select><br>');
                        $.each(ticketsData, function (key, value) {
                            if ($.type(value) !== "undefined") {
                                var isSelected = key == properties.totals.ticketsPerType.ticketIds[ticketTypes] ? 'selected' : '';
                                html.find('select').append('<option value="' + key + '" ' + isSelected + '>' + value + '</option>');
                            }
                        });
                        html.append('<label>Email</label><br>');
                        html.append('<input placeholder="Например: test@test.test" class="form-control" type="email" name="form_' + i + '[email]"><br>');
                        html.append('<label>Имя</label><br>');
                        html.append('<input placeholder="Например: Иван" class="form-control" type="text" name="form_' + i + '[name]"><br>');
                        html.append('<label>Фамилия</label><br>');
                        html.append('<input placeholder="Например: Иванов" class="form-control" type="text" name="form_' + i + '[surname]"><br>');
                        html.append('<label>Номер телефона</label><br>');
                        html.append('<input placeholder="+375 (__) ___-__-__" class="form-control phone-input" type="text" name="form_' + i + '[phone]"><br>');
                        wrapper.append(html);
                        i++;
                    }
                }
                wrapper.append('<button type="submit" class="btn btn-primary">Отправить заявку</button>');
                $(this).attr('disabled', 'disabled');
                wrapper.show();
                methods.setPhoneMask();

                $(properties.removeMemberSelector).on('click', methods.removeMember);
            },
            setPhoneMask: function() {
                $(".phone-input").mask("375999999999");
            },
            removeMember: function() {
                $(this).parent().remove();
            }
        };

        return methods;
    };

    initTicketsPopup().run();
    
})(jQuery);