<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Festivals */

$this->title = 'Создать фестиваль';
$this->params['breadcrumbs'][] = ['label' => 'Управление Фестивалями', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
