<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Festivals */

$this->title = 'Редактирование фестиваля: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Фестивали', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="festivals-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
