<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фестивали';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить фестиваль', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'description',
            'short_description',
            'location',
            // 'logo',
            // 'order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>