<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poster */

$this->title = 'Редактирование мероприятия: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="poster-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
