<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Festivals;
use app\models\User;
use app\components\FormsHelper;
use app\components\SiteHelper;
use kartik\date\DatePicker;
use kartik\markdown\MarkdownEditor;
use dosamigos\multiselect\MultiSelect;
//use dosamigos\multiselect\MultiSelect;

/* @var $this yii\web\View */
/* @var $model app\models\Poster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poster-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'festivalId')->dropDownList(ArrayHelper::map(Festivals::find()->all(), 'id', 'name'))->label('Фестиваль'); ?>

    <?= $form->field($model, 'curators')->dropDownList($curat = ArrayHelper::map(User::find()->where(['privilege' => 50])->all(), 'id', 'username'),
        [
            'multiple' => 'multiple'
        ]

    )->label('Куратор'); ?>
    <? $form->field($model, 'curators')->widget(MultiSelect::className(),[
        'data' => ['super', 'natural'],
    ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название мероприятия') ?>

    <label class="control-label">Описание</label>
    <?php echo MarkdownEditor::widget([
        'model' => $model,
        'attribute' => 'description',
    ]); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Выберите дату проведения мероприятия'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]); ?>

    <?= $form->field($model, 'location_from')->textInput(['maxlength' => true])->label('Города участники') ?>

    <?= $form->field($model, 'location_to')->textInput(['maxlength' => true])->label('Место проведения') ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true])->label('Площадка') ?>

    <?= $form->field($model, 'imageFile')->fileInput()->label('Логотип') ?>

    <?php FormsHelper::getCheckboxes(SiteHelper::getRequestsFields(), "Поля для формы заявки", $model->form_fields, $model->form_required_fields); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
