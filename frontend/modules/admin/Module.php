<?php

namespace app\modules\admin;

use Yii;
use common\models\User;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function beforeAction($action){

        if (!parent::beforeAction($action)) {
            return false;
        }

        if (User::IsAdmin() || User::IsModerator()){
            return true;
        } else {
            Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
            return false;
        }

    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
