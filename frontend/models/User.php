<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $phone_number
 * @property integer $privilege
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $social
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['privilege', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'name', 'surname', 'phone_number', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['social'], 'string', 'max' => 500],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone_number' => 'Номер телефона',
            'privilege' => 'Привилегия',
            'auth_key' => 'Код авторизации',
            'password_hash' => 'Хеш пароля',
            'password_reset_token' => 'Идентификатор сброса пароля',
            'email' => 'Почтовый ящик',
            'social' => 'Социальное',
            'status' => 'Статус',
            'created_at' => 'Создан с',
            'updated_at' => 'Обновлен с',
        ];
    }
}
