<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property string $author
 * @property string $festival_id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $email
 * @property string $phone_number
 * @property string $document_type
 * @property string $document_number
 * @property string $date_of_issue
 * @property string $who_gave
 * @property string $validity
 * @property string $status
 * @property string $ticket_id
 */
class Requests extends \yii\db\ActiveRecord
{

    public $rules;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Author',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'email' => 'Email',
            'phone_number' => 'Номер телефона',
            'document_type' => 'Тип документа',
            'document_number' => 'Серия и Номер',
            'date_of_issue' => 'Дата выдачи',
            'who_gave' => 'Кем выдан',
            'validity' => 'Срок действия',
            'status' => 'Статус',
            'ticket_id' => 'ticket id',
        ];
    }

    public function __construct(){
        parent::__construct();

        $this->rules = [
            [['ticket_id', 'author', 'festival_id', 'status'], 'required'],
            [['name', 'surname', 'patronymic', 'email', 'phone_number', 'document_type', 'document_number', 'date_of_issue', 'who_gave', 'validity'], 'required']
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        Yii::$app->mailer->compose()
            ->setFrom('email@bust.by')
            ->setTo($this->email)
            ->setSubject('Test message')
            ->setTextBody('Test body')
            ->send();

        return true;
    }
}
