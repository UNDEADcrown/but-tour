<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property integer $id
 * @property integer $festival_id
 * @property string $type
 * @property string $description
 * @property string $price
 * @property integer $status
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const TICKET_STATUS_DISABLE = 0;
    const TICKET_STATUS_ENABLE = 1;

    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festival_id', 'status'], 'integer'],
            [['type', 'price'], 'required'],
            [['type', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'festival_id' => 'ID Фестиваля',
            'type' => 'Тип билета',
            'description' => 'Описание',
            'price' => 'Цена билета',
            'status' => 'Статус',
        ];
    }

    public function getStatuses() {
        return [
            [
                "id" => self::TICKET_STATUS_DISABLE,
                "name" => 'Отключен'
            ],
            [
                "id" => self::TICKET_STATUS_ENABLE,
                "name" => 'Включен'
            ]
        ];
    }

    public static function getTicketsByFestivalId($id) {
        return Tickets::find()->where(['festival_id' => $id])->all();
    }
}
