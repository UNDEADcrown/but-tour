<?php

namespace app\models;

use Yii;
use Yii\helpers\FileHelper;

/**
 * This is the model class for table "festivals".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property string $location
 * @property string $logo
 * @property integer $order
 */
class Festivals extends \yii\db\ActiveRecord
{

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'festivals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['order'], 'integer'],
            [['name', 'description', 'short_description', 'location', 'logo'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'short_description' => 'Короткое описание',
            'location' => 'Место проведения',
            'logo' => 'Логотип',
            'order' => 'Порядок сортировки',
        ];
    }

    public function upload(){
        $this->logo = $this->imageFile->name;
        if($this->validate()){
            $path = 'uploads/festivals/';
            FileHelper::createDirectory($path);
            $this->imageFile->saveAs($path . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->imageFile = '';
            return true;
        } else {
            return false;
        }
    }
}
