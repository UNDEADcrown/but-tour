<?php

namespace app\models;

use Yii;
use Yii\helpers\FileHelper;

/**
 * This is the model class for table "poster".
 *
 * @property integer $id
 * @property integer $festivalId
 * @property string $date
 * @property string $name
 * @property string $description
 * @property string $location_from
 * @property string $location_to
 * @property string $area
 * @property string $avatar
 * @property string $form_fields
 * @property string $form_required_fields
 */
class Poster extends \yii\db\ActiveRecord
{

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poster';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festivalId', 'date', 'location_from', 'name', 'description', 'curators'], 'required'],
            [['festivalId'], 'integer'],
            [['date', 'location_from', 'location_to', 'area', 'avatar', 'name'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'festivalId' => 'Festival ID',
            'date' => 'Дата проведения',
            'name' => 'Название мероприятия',
            'description' => 'Описание мероприятия',
            'location_from' => 'Города участники',
            'location_to' => 'Место проведения',
            'area' => 'Площадка',
            'avatar' => 'Аватар',
            'curators' => 'Кураторы',
        ];
    }

    public function upload(){
        $this->avatar = $this->imageFile->name;
        if($this->validate()){
            $path = 'uploads/posters/';
            FileHelper::createDirectory($path);
            $this->imageFile->saveAs($path . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->imageFile = '';
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);

        $postData = Yii::$app->request->post();
        $formFields = "";
        $formRequiredField = "";

        $this->curators = serialize($this->curators);


        foreach($postData["checkboxarray"] as $key => $value) {
            $formFields .= $key . "|";
        }

        if(!empty($postData["checkboxarrarequired"])) {
            foreach ($postData["checkboxarrarequired"] as $key => $data) {
                $formRequiredField .= $key . "|";
            }
        }

        $this->form_fields = $formFields;
        $this->form_required_fields = $formRequiredField;

        return true;
    }

    public function afterFind(){

        $this->curators = unserialize($this->curators);
    }

    public function getPosterBackground() {
        return '@web/images/poster-default-logo.jpg';
    }


}
