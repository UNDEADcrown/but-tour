<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use app\models\Festivals;
use app\models\Poster;
use app\models\Requests;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => array(
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('login'),
            ),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->actionPosters();
        //return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
                    $identity = User::findByEAuth($eauth);
                    Yii::$app->user->login($identity, 3600 * 24 * 30);
                    $eauth->redirect();
                }
                else {
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
                $eauth->redirect($eauth->getCancelUrl());
            }
        } else {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            } else {
                return $this->render('login', [
                    'model' => $model,
                ]);
            }

        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionFestivals(){

        $festivalCollection = new Festivals();

        return $this->render('festivals', [
            'collection' => $festivalCollection::find()->all(),
        ]);

    }

    public function actionFestival($id){

        $festivalCollection = new Festivals();

        return $this->render('festival', [
            'festival' => $festivalCollection::findOne($id),
        ]);

    }

    public function actionPoster($id){

        $posterCollection = new Poster();

        return $this->render('poster', [
            'poster' => $posterCollection::findOne($id),
        ]);

    }

    public function actionPosters(){

        $posterCollection = new Poster();


        $postData = Yii::$app->request->post();

        if(Yii::$app->request->post()){
            $saved = 0;
            //TODO: Add js validation
            foreach ($postData as $item) {
                if(empty($item['ticket_type'])) continue;
                $requestModel = new Requests();
                //TODO: Need check if user is logged
                $requestModel->author = 0;
                $requestModel->status = 1;
                //TODO: Need send fetival id in the hidden input
                $requestModel->festival_id = 0;
                $requestModel->patronymic = 'NULL';
                $requestModel->patronymic = 'NULL';
                $requestModel->name = $item['name'];
                $requestModel->surname = $item['surname'];
                $requestModel->email = $item['email'];
                $requestModel->phone_number = $item['phone'];
                $requestModel->ticket_id = $item['ticket_type'];
                $requestModel->document_type = 'NULL';
                $requestModel->document_number = 'NULL';
                $requestModel->date_of_issue = 'NULL';
                $requestModel->who_gave = 'NULL';
                $requestModel->validity = 'NULL';
                if($requestModel->save()) $saved++;
            }

            if($saved){
                Yii::$app->getSession()->setFlash('success', 'Заявка успешно отправлена.');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Возникла неизвестная ошибка, пожалуйста, свяжитесь с нами и расскажите о случившемся.');
            }
        }

        Yii::$app->mailer->compose()
            ->setFrom('email@bust.by')
            ->setTo('email@bust.by')
            ->setSubject('Test message')
            ->setTextBody('Test body')
            ->send();

        return $this->render('posters', [
            'collection' => $posterCollection::find()->all(),
        ]);

    }
}
